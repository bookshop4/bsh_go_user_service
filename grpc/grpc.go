package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/bookshop/bsh_go_user_service/config"
	"gitlab.com/bookshop/bsh_go_user_service/genproto/order_service"
	"gitlab.com/bookshop/bsh_go_user_service/grpc/client"
	"gitlab.com/bookshop/bsh_go_user_service/grpc/service"
	"gitlab.com/bookshop/bsh_go_user_service/pkg/logger"
	"gitlab.com/bookshop/bsh_go_user_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	order_service.RegisterOrderServiceServer(grpcServer, service.NewOrderService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
